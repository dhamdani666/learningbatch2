<?php

namespace App\Http\Controllers;
use App\Barang;

use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function getNama($nama)
    {
        $barang = Barang::where('nama', $nama)->first();
        if(is_null($barang)){
            return 'barang tidak tersedia';
        }
        return 'Nama Barang Adalah :' . $barang->nama;

    }

    public function getAlamat()
    {
        return 'Bandung';

    }
}
