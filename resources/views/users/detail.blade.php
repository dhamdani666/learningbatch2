@extends('template',['title'=>'Detail Data Users'])
@section('content')
    <div class="card">
        <div class="card-header">
            Detail Data Users
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" value="{{$dataUsers->username}}" readonly>
                </div>
                <div class="form-group col-md-4">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control" value="{{$dataUsers->nama}}" readonly>
                </div>
                <div class="form-group col-md-4">
                    <label>Profile</label> <br>
                    <img src="{{URL('images/users/'.$dataUsers->profile.'')}}" class="img-responsive" width="150"
                         height="150">
                </div>
            </div>

            <hr>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label>Tanggal Buat</label>
                    <input type="text" name="category" class="form-control" value="{{$dataUsers->created_at}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label>User Buat</label>
                    <input type="text" name="nama" class="form-control" value="{{$dataUsers->created_by}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label>Tanggal Ubah</label>
                    <input type="text" name="stock" class="form-control" value="{{$dataUsers->updated_at}}" readonly>
                </div>
                <div class="form-group col-md-3">
                    <label>User Ubah</label>
                    <input type="text" name="stock" class="form-control" value="{{$dataUsers->updated_by}}" readonly>
                </div>
            </div>
            <div class="text-right">
                <a href="{{url('users')}}" class="btn btn-success">
                    <i class="fa fa-chevron-left"></i> Kembali
                </a>
            </div>
        </div>
    </div>
@endsection